import {  useEffect, Fragment, useContext } from 'react'
import Router from 'next/router'
import View from '../../components/View'
import UserContext from '../../UserContext.js'

export default () => {
  	
  	const {unsetUser} = useContext(UserContext)

  	useEffect(() => {
  		unsetUser()
  		Router.push('/') // redirect user back into the login page.
  	}, [])

    return (
      <div id="main">
        <View title="Logout">
            <h5 className="text-center">Logging out...</h5>
            <h6 className="text-center">You will be redirected back to the login page shortly.</h6>   
        </View>
      </div>
       
    )
}
