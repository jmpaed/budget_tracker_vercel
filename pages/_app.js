import {useState, useEffect} from 'react';
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import NavBar from '../components/NavBar.js'
import Container from 'react-bootstrap/Container'
import {UserProvider} from '../UserContext.js'
import AppHelper from '../apphelper.js'
import BgVid from '../components/BgVid.js'


function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({email: null})

	const unsetUser = () => {
		localStorage.clear()
		setUser({ email: null })
	}

	useEffect(() => {
		if(AppHelper.getAccessToken() !== null){
			const options = {
				headers: { 'Authorization': `Bearer ${AppHelper.getAccessToken() }`}
			}
			fetch(`https://budget-tracker-by-jm.herokuapp.com/api/users/details`, options)
			.then(response => response.json())
			.then((userData) => {
				if(typeof userData.email != "undefined"){
					setUser({ email: userData.email })
				}else{
					// if the condition was not met, meaning, the userData is null or undefined.
					setUser ({ email: null })
				}
			})
		}else{
			console.log("No Authenticated user")
		}
	}, [user.id])

  return (
  	<>
  	<UserProvider value={{user, setUser, unsetUser}}>
  	<BgVid />
  	<NavBar />
  	<Container>
  		<Component {...pageProps} />
  	</Container>
  	</UserProvider>
  	</>
  	)
}

export default MyApp
