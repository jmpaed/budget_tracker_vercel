import {useState, useEffect} from 'react';
import {Row, Col, Form, Button, Container} from 'react-bootstrap';
import Head from 'next/head';
import AppHelper from '../apphelper.js';
import Google from '../components/Google.js';
import Router from 'next/router';
import Swal from 'sweetalert2';
import Link from 'next/link'


export default function Register(){

	const [firstName, setFirstName] = useState("");

	const [lastName, setLastName] = useState("");

	const [email, setEmail] = useState("");

	const [mobileNo,setMobileNo] = useState(0)

	const [pass, setPass] = useState("");

	const [verify, setVerify] = useState("");

	const [disable, setDisabled] = useState(false);

	// state for conditional rendering of the submit button


  	useEffect(() => {
		if((firstName.length != 0 && lastName.length != 0 && email.length != 0 && pass.length != 0 && verify.length != 0) && (pass === verify) && (mobileNo.length === 11))
		{
			setDisabled(false);
		}else {
			setDisabled(true);
		}
	}, [firstName, lastName, mobileNo, email, pass, verify])

	function registerUser(e) {
		e.preventDefault() //to avoid page redirection.
		//lets check if an email exist in our records. 
		fetch('https://budget-tracker-by-jm.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			}) //convert the value to string data type to be accepted in the API
		}).then(res => res.json()).then(data => {
			//lets create a checker just to see if may nakukuha tayo na data
			console.log(data)
			//lets create a control structure to give the appropriate response according to the value of the data.
			if(data === false){
				//if the return in false then allow the user to register otherwise NOPE
				fetch('https://budget-tracker-by-jm.herokuapp.com/api/users/register', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: pass
					})
				}).then(res => res.json()).then(data => {
					//you can create a checker here if you wish
					console.log(data)
					if(data) {
						Swal.fire({
							icon: 'success',
							title: 'Successfully Registered',
							text: 'Thank you for registering.'
						})
						//after displaying a success message redirect the user to the login page
						Router.push('/')
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Registration failed',
							text: 'Something went wrong'
						})
					}
				})
			} else {
				//the else branch will run if there return value is true
				Swal.fire({
					icon: 'error',
					title: 'Registration Failed',
					text: 'Email is already taken by someone else.'
				})
			}
		})
	}

	return (
		<>
			<div id="main">
			<h1 className="mt-5 pt-5 text-center">Register</h1>
			<Form className="mb-3" onSubmit={e => registerUser(e)}>
				<Form.Group controlId="userFirstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="userLastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="userEmail">
					<Form.Label>Email</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="mobileNo">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control type="number" placeholder="Enter Mobile No." value={mobileNo} onChange={e => setMobileNo(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={pass} onChange={e => setPass(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" value={verify} onChange={e => setVerify(e.target.value)} required/>
				</Form.Group>
					
				<Button variant="primary" disabled={disable} type="submit" className="btn-block mb-3">Register</Button>
				
			</Form>
			</div>
			<div className="notyet">Already Registered? Click <Link href="/">HERE</Link>.</div>
		</>
		)	
}