import { useState, useEffect } from 'react'
import { Table, Button } from 'react-bootstrap'
import Link from 'next/link'
import View from '../../../components/View'
import AppHelper from '../../../apphelper.js'

export default () => {
   
    const [categories, setCategories] = useState([])



    useEffect(() => {
        const payload = {
            method: 'GET',
            headers: {
                'Content-Type':'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            }
        }
        
        fetch(`https://budget-tracker-by-jm.herokuapp.com/api/users/user-categories`, payload)
            .then(res => res.json())
            .then(data => {
                console.log(data)

                data.categories.map(category => {
                    console.log(category)
                    setCategories(category)
                })
                

            })
    }, [])
    


    return (
        <div id="main" className="categoriesPage">
        <View title="Categories">
            <h3>Categories</h3>
            <Link href="/user/categories/new"><a className="btn btn-success mt-1 mb-3">Add</a></Link>
            <Table striped bordered hover>
                <thead>
                    <tr className="categoriesPage">
                        <th>Category</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody className="categoriesPage">


                  
                    {categories.map((category) => {
                    const textColor = (category.type === 'Income') ? 'text-success' : 'text-danger'
                    return (
                        <tr key={category._id}>
                            <td><strong>{category.name}</strong></td>
                            <td className = {textColor}>{category.type}</td>
                        </tr>
                    )
                })}
                  
                </tbody>
            </Table>
        </View>
        </div>
    )
}
