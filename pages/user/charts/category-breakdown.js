import { useState, useEffect } from 'react';
import { InputGroup, Form, Col } from 'react-bootstrap';
import { Pie } from 'react-chartjs-2';
import View from '../../../components/View';
import moment from 'moment';
import AppHelper from '../../../apphelper';
import {colorRandomizer} from '../../../randomcolor.js'


 const Category = () => {
   
    const [labelsArr, setLabelsArr] = useState([])
    const [dataArr, setDataArr] = useState([])
    const [bgColors,setBgColors] = useState([])

    const [fromDate, setFromDate] = useState(moment().subtract(1, 'months').format('YYYY-MM-DD'))
    const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'))
  
    useEffect(() => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            }, 
            body: JSON.stringify({
                fromDate: fromDate,
                toDate: toDate
            })
        }
        fetch(`https://budget-tracker-by-jm.herokuapp.com/api/users/get-records-breakdown-by-range`, payload)
        .then(res => res.json())
        .then(data => {
            setLabelsArr(data.map(record => record.categoryName))
            setDataArr(data.map(record => record.totalAmount))
            setBgColors(data.map(()=> `#${colorRandomizer()}`))

          
           
        })

    }, [fromDate, toDate])

  

    const data = {
        labels: labelsArr,
        datasets: [
            {
                data: dataArr,
                backgroundColor: bgColors,
                hoverBackgroundColor: bgColors
            }
        ]
    };

    return (
        <View title="Breakdown Summary">
            
                   
                    <form >
                    <h3 className="categoriesPage">Category Breakdown</h3>
                            <label>From</label>
                            <input type="date" value={fromDate} onChange={(e) => setFromDate(e.target.value) }/>
                    
                    
                            <label>To</label>
                            <input type="date" value={toDate} onChange={(e) => setToDate(e.target.value) }/>
                    
                    </form>
           
        

            
            <div>
            <Pie data={data} />

           
            </div>
        </View>

    )
}

export default Category;

