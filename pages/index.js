import { useState, useEffect, useContext } from 'react'
import { Form, Button, Card, Row, Col, Container} from 'react-bootstrap'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import View from '../components/View'
import Swal from 'sweetalert2';
import Router from 'next/router'
import UserContext from '../UserContext.js'
import Link from 'next/link'
/*import { GoogleLogin } from 'react-google-login';*/


export default function Home() {


  const [email, setEmail] = useState("")

  const [pass, setPass] = useState("")

  const [disabled, setDisabled] = useState(false)

  const {user, setUser} = useContext(UserContext)

    if(user.email !== null) {
      Router.push('/user/records');
    }

  /*const cID = "1067337672654-ldkiq01b785m6k0tkr2uaspjpnigaja4.apps.googleusercontent.com"

  const responseGoogle = (response) => {

  console.log(response);

  const payload = {
            method: 'POST',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({ tokenId: response.tokenId })
        }
        fetch(`http://localhost:4000/api/users/verify-google-id-token`, payload)
        .then(response => response.json())
        .then(data => {
            console.log(data)
        })
}*/


  // let's create a function to retrieve the use details before the client can be redirected inside the records page

  const retrieveUserDetails = (accessToken) => {
    // make sure the user has already been verified, meaning it was already granted an access token.
    const options = {
      headers: { Authorization: `Bearer ${accessToken}` }
    }
    // create a request going to the desired endpoint with the payload.
    fetch('https://budget-tracker-by-jm.herokuapp.com/api/users/details', options).then((response) => response.json()).then(data => {
      setUser({ email: data.email })
      Router.push('/user/records')
    })
  } 



  useEffect(() => {
    if(email.length === 0 || password.length === 0)
    {
      setDisabled(true);
    }else {
      setDisabled(false);
    }
  }, [email, pass])

  function login(e){
    e.preventDefault()
    //describe the request to login
    fetch("https://budget-tracker-by-jm.herokuapp.com/api/users/login", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: pass
      }) //for the request to be accepted by the api
    }).then(res => res.json()).then(data => {
      console.log(data)
      //lets create a control structure
      if(typeof data.accessToken !== "undefined") {
        //store the access token in the local storage
        localStorage.setItem('token', data.accessToken)
        retrieveUserDetails(data.accessToken)
        Swal.fire({
          icon: 'success',
          title: 'Successfully Logged In'
        })
    
      } else {
           Swal.fire('Login Error', 'You may have registered using a different login procedure', 'error')
      }
    })
  }     
    


  return (
    <>
    <div id="main">
    <View title={ 'Budget Tracking App' }>
    <div id="loginText">
      <h1>Welcome to Budget Tracker App by JM</h1>
      <p>Login by using your Registered Email.</p>
    </div>
          <Form onSubmit={e => login(e)}>
              <Form.Group controlId="email">
                  <Form.Label>Email:</Form.Label>
                  <Form.Control type="text" value={email} onChange={e=>setEmail(e.target.value)} required/>
                  <Form.Text className="text-muted loginText">
                    We'll never share your email with anyone else.
                  </Form.Text>
              </Form.Group>
              <Form.Group controlId="password">
                  <Form.Label>Password:</Form.Label>
                  <Form.Control type="password" value={pass} onChange={e=>setPass(e.target.value)} required/>
              </Form.Group>
              <Button type="submit" variant="dark" className="btn-block mb-3">Submit</Button>
          </Form>
          {/*<GoogleLogin
            clientId={cID}
            onSuccess={responseGoogle}
            onFailure={responseGoogle}
            cookiePolicy={'single_host_origin'}
            theme="dark"
            className="btn-block mb-3"
          />*/}
          <br />
          <div className="notyet">
            <p>Not yet registered? click <Link href='/register'>HERE</Link>.</p>
          </div>
    </View>

    <div>
      <footer className={styles.footer}>
              <p>Budget Tracker App by JM</p>  
        </footer>
    </div>
    </div>
    </>
  )
}


// https://budget-tracker-by-jm.herokuapp.com/api/users/login