
export default function BgVid() {
	return (
		<>
			<div id="BgV"> 
    			<video className='videoTag' autoPlay loop muted>
    				<source src="/bgVid.mp4" type='video/mp4' />
    			</video>
    		</div>
		</>
		)
}