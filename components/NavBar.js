import {Navbar, Nav} from 'react-bootstrap'
import { useContext, useState } from 'react'
import Link from 'next/link'
import UserContext from '../UserContext.js'
  

export default function NavBar() {

  const {user} = useContext(UserContext)

  const [isExpanded, setIsExpanded] = useState(false)

  let RightNavOptions;
  let LeftNavOptions;

  // let's create a control structure which will determine the options displayed in the navbar.

  if(user.email !== null){
    RightNavOptions = (
        <>
          <Link href="/logout">
            <a className="nav-link">Logout</a>
          </Link>
        </>
      )
    LeftNavOptions = (
      <>
          <Link href="/user/categories">
            <a className="nav-link">Categories</a>
          </Link>    
          <Link href="/user/records">
            <a className="nav-link">Records</a>
          </Link>
          <Link href="/user/charts/category-breakdown">
            <a className="nav-link">Breakdown</a>
          </Link>        
      </>
    )
  }else{
    RightNavOptions = (
        <Link href="/register">
            <a className="nav-link">Register</a>
          </Link>
      )
    LeftNavOptions = null
  }

	return (
		<Navbar expanded={ isExpanded } expand="lg" id="nav" bg="dark" variant="dark">
  			<Navbar.Brand href="/">Budget Tracker by JM</Navbar.Brand>
  			<Navbar.Toggle aria-controls="basic-navbar-nav" onClick={ () => setIsExpanded(!isExpanded) }/>
  			<Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto" onClick={ () => setIsExpanded(!isExpanded) }>
              {/*<UserLinks />*/}
              {LeftNavOptions}
          </Nav>
    			<Nav className="ml-auto" onClick={ () => setIsExpanded(!isExpanded) }>
      				{/*<UserLinks />*/}
              {RightNavOptions}
    			</Nav>
  			</Navbar.Collapse>
		</Navbar>
		)
}

/*export function UserLinks(){

  const {user} = useContext(UserContext)

  if(user.email !== null) {
    return <Link href="/logout"><a className="nav-link">Log Out</a></Link>
  }
  return (
    <Link href="/register"><a className="nav-link">Register</a></Link>
    )
}*/