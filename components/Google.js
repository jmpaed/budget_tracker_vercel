import AppHelper from '../apphelper.js';
import { GoogleLogin, GoogleLogout } from 'react-google-login';

const retrieveGoogleDetails = (res) => {
		console.log(res);

		const payload = {
            method: 'POST',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({ tokenId: res.tokenId })
        }
        fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
        .then(res => res.json())
        .then(data => {
            console.log(data)
        })
	}

const cID = "1067337672654-gg5jh30p551nn1firq0gf6c8u679598i.apps.googleusercontent.com"

export default function GoogleBtn() {
	return (
		<GoogleLogin 
					clientId={cID}
					onSuccess={retrieveGoogleDetails}
	    			onFailure={retrieveGoogleDetails}
	    			cookiePolicy={'single_host_origin'}
	    			theme="dark"
					/>
					)
}