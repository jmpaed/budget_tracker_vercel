// the goal of this module is to shorten written code in various parts of our project.
module.exports = {
	API_URL: process.env.NEXT_PUBLIC_API_URL,
	getAccessToken: () => localStorage.getItem('token'),
	
}

